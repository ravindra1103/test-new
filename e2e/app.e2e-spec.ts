import { TestNewPage } from './app.po';

describe('test-new App', () => {
  let page: TestNewPage;

  beforeEach(() => {
    page = new TestNewPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
